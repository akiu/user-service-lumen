<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userService = app('UserService');
    }

    public function create(Request $request)
    {
        $user = $this->userService->create($request->all());
        return response()->json($user);
    }
}
