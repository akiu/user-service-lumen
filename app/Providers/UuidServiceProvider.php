<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UuidServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Uuid', function($app) {
        	return \Ramsey\Uuid\Uuid::uuid4();
        });
    }

    public function boot()
    {

    }
}
