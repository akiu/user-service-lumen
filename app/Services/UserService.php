<?php

namespace App\Services;

use App\Models\User;

class UserService
{
	protected $uuid;

	public function __construct()
	{
		$this->uuid = app('Uuid');
	}

	public function create($input)
	{
		$user = new User();
		$user->id = $this->uuid->toString();
		$user->username = $input['username'];
		$user->email = $input['email'];
		$user->password = app('hash')->make($input['password']);
		$user->save();

		return $user;
	}

	public function checkPassword($hashed, $plain)
	{

	}
}

?>